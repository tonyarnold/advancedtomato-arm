#!/bin/sh

#
# Copyright (C) 2015 shibby
#

MODE=`nvram get wan_proto`

if [ ! "$MODE" == "lte" ]; then
    exit 0
fi

if [ "$1" == "connect" ]; then
    APN=`nvram get modem_apn`
    DEV=`nvram get modem_dev4g`
    TYP=`nvram get modem_type`
    IFA=`nvram get wan_4g`

    logger 4G MODEM - connecting ...

    if [ "$TYP" == "nonhilink" ]; then
        CONNECTED=0

        while [ $CONNECTED == "0" ]; do
            MODE="AT^NDISDUP=1,1,\"$APN\"" gcom -d $DEV -s /etc/gcom/setverbose.gcom > /tmp/4g.mode

            MODE="AT+CGCONTRDP" gcom -d $DEV -e -s /etc/gcom/setverbose.gcom > /tmp/4g.check
            CHECK1=`cat /tmp/4g.check | grep ERROR | wc -l`
            CHECK2=`cat /tmp/4g.check | grep open | wc -l`
            if [ "$CHECK1" == "1" ]; then
                logger 4G MODEM - device $DEV not connected yet ...
                sleep 5
            elif [ "$CHECK2" == "1" ]; then
                logger 4G MODEM - device $DEV not ready ...
                sleep 5
            else
                logger 4G MODEM - connected ...
                CONNECTED=1
            fi
        done
    fi

    GO=0
    while [ $GO = "0" ]; do
        dhcpc-renew

        CHECKIFA=`ifconfig | grep $IFA | wc -l`
        if [ "$CHECKIFA" == "1" ]; then
            GO=1
            logger 4G MODEM - WAN configured ...
        fi
    done

elif [ "$1" == "disconnect" ]; then
    DEV=`nvram get modem_dev4g`

    logger 4G MODEM - disconnecting ...

    MODE="AT^NDISDUP=1,0" gcom -d $DEV -s /etc/gcom/setmode.gcom
else

    LOCK="/tmp/switch4g.lock"
    if [ -f $LOCK ]; then #lock exist
        logger 4G MODEM - previous proces of switch4g still working
        exit 0
    else

        PIN=`nvram get modem_pin`
        IS_PIN=`nvram get modem_pin | wc -w`

        modprobe cdc_ether
        modprobe cdc_ncm

        # is modem ready?
        NH=`cat /proc/bus/usb/devices | grep Driver | grep cdc_ncm | wc -l`
        H=`cat /proc/bus/usb/devices | grep Driver | grep cdc_ether | wc -l`
        US=`cat /proc/bus/usb/devices | grep Driver | grep usbserial | wc -l`

        if [ "$H" -gt 0 ]; then
            logger 4G MODEM already found - Hilink - using cdc_ether module
            nvram set 4g_module=cdc_ether
            rm $LOCK
        elif [ "$NH" -gt 0 ]; then
            logger 4G MODEM already found - Non-Hilink - using cdc_ncm module
            nvram set 4g_module=cdc_ncm
            rm $LOCK
        fi

        if [ "$US" -gt 0 ]; then
            logger 4G MODEM already found - Diagnostic interface - using usbserial module
            rm $LOCK
        else
            #modem not found, try detect

            DEVICES=`lsusb | awk '{print $6}'`

            for SWITCH in $DEVICES; do
                SEARCH=`ls /etc/usb_modeswitch.d/$SWITCH | wc -l`

                # vendor:product

                if [ "$SEARCH" == "1" ]; then
                    logger 4G MODEM FOUND - $SWITCH - Switching ...
                    DV=`echo $SWITCH | cut -d ":" -f1`
                    DP=`echo $SWITCH | cut -d ":" -f2`
                    /usr/sbin/usb_modeswitch -Q -c /etc/usb_modeswitch.d/$SWITCH -v $DV -p $DP

                    TEST1=`cat /etc/usb_modeswitch.d/$SWITCH | grep "TargetVendor" | cut -d "=" -f2 | wc -l`
                    if [ "$TEST1" == "1" ]; then
                        VENDOR=`cat /etc/usb_modeswitch.d/$SWITCH | grep "TargetVendor" | cut -d "=" -f2 | cut -d "x" -f2`
                    else
                        VENDOR=`echo $SWITCH | cut -d ":" -f1`
                    fi

                    TEST2=`lsusb | awk '{print $6}' | grep $VENDOR | wc -l`
                    if [ "$TEST2" == "1" ]; then
                        PRODUCT=`lsusb | awk '{print $6}' | grep $VENDOR | cut -d ":" -f2`
                        logger 4G MODEM ready - $VENDOR:$PRODUCT
                        echo "$VENDOR:$PRODUCT" > /tmp/3g.detect
                    fi
                fi
            done

            IS_VENDOR=`echo $VENDOR | wc -w`
            if [ "$IS_VENDOR" -gt 0 ]; then
                IS_PRODUCT=`echo $PRODUCT | wc -w`
                if [ "$IS_PRODUCT" -gt 0 ]; then
                    logger 4G MODEM - loading module usbserial
                    rmmod usbserial
                    insmod usbserial vendor=0x$VENDOR product=0x$PRODUCT
                    echo "$VENDOR:$PRODUCT" > /tmp/4g.detect
                fi
            fi

            DEV=`cat /proc/bus/usb/devices | grep Driver | grep usbserial | wc -l`
            if [ "$DEV" -gt 0 ]; then
                logger 4G MODEM ready - using usbserial module
                nvram set 4g_module=usbserial
                rm $LOCK
                break;
            elif [ -f /tmp/4g.detect ]; then
                VENDOR=`cat /tmp/4g.detect | cut -d ":" -f1`
                PRODUCT=`cat /tmp/4g.detect | cut -d ":" -f2`
                TEST3=`lsusb | grep $VENDOR | grep $PRODUCT | wc -l`
                if [ "$TEST3" == "1" ]; then
                    logger 4G MODEM FOUND - already switched - Last know $VENDOR:$PRODUCT

                    DEV=`cat /proc/bus/usb/devices | grep Driver | grep usbserial | wc -l`
                    if [ "$DEV" -gt 0 ]; then
                        logger 4G MODEM ready - using usbserial module
                        nvram set 4g_module=usbserial
                        rm $LOCK
                        break;
                    else
                        logger 4G MODEM - loading module
                        rmmod usbserial
                        insmod usbserial vendor=0x$VENDOR product=0x$PRODUCT
                        echo "$VENDOR:$PRODUCT" > /tmp/4g.detect
                        nvram set 4g_module=usbserial
                        rm $LOCK
                        break;
                    fi
                fi
            else
                #last change. try load usbserial for each usb devices
                DEVICES=`lsusb | awk '{print $6}'`
                for SWITCH in $DEVICES; do
                    VENDOR=`echo $SWITCH | cut -d ":" -f1`
                    PRODUCT=`echo $SWITCH | cut -d ":" -f2`
                    rmmod usbserial
                    insmod usbserial vendor=0x$VENDOR product=0x$PRODUCT

                    DEV=`cat /proc/bus/usb/devices | grep Driver | grep usbserial | wc -l`
                    if [ "$DEV" -gt 0 ]; then
                        logger 4G MODEM ready - using usbserial module
                        nvram set 4g_module=usbserial
                        echo "$VENDOR:$PRODUCT" > /tmp/4g.detect
                        rm $LOCK
                        break;
                    fi
                done
            fi
        fi

        #search diagnostic device
        TTY=`ls /dev/ttyUSB*`

        for i in $TTY; do
            CHECKTTY=`gcom -d $i -e info | grep OK | wc -l`
            if [ "$CHECKTTY" == "1" ]; then #found working interface
                nvram set modem_dev4g=$i
                DEVNR=$i
            fi
        done

        #search WAN interface (usbX or ethX)
        HWAN=`dmesg | grep cdc_ether | grep register | grep "'" | cut -d " " -f3 | cut -d ":" -f1`
        NHWAN=`dmesg | grep cdc_ncm | grep register | grep "'" | cut -d " " -f3 | cut -d ":" -f1`

        IS_HWAN=`echo $HWAN | wc -w`
        IS_NHWAN=`echo NHWAN | wc -w`

        if [ "$IS_HWAN" -gt 0 ]; then
            logger 4G MODEM WAN found - Hilink - using $HWAN as WAN
            nvram set wan_4g="$HWAN"
            nvram set modem_type=hilink
        elif [ "$IS_NHWAN" -gt 0 ]; then
            logger 4G MODEM WAN found - Non-Hilink - using $NHWAN as WAN
            nvram set wan_4g="$NHWAN"
            nvram set modem_type=nonhilink
        else
            logger 4G MODEM WAN not found - exit
            break;
        fi

        #set pin
        if [ "$IS_PIN" == "1" -a "$NH" -gt 0 ]; then #only for non-hilink
            PINCODE="$PIN" gcom -d $DEVNR -s /etc/gcom/setpin.gcom
        fi

        if [ "$NH" -gt 0 ]; then #only for non-hilink
            #check signal strength
            CSQ=`gcom -d $DEVNR -s /etc/gcom/getstrength.gcom | grep "CSQ:" | cut -d " " -f2 | cut -d "," -f1`
            DBM=$((-113+CSQ*2))
            logger "4G MODEM Signal Strength: $DBM dBm"
        fi

        #force connection after detect 4G modem
        switch4g connect

    fi

#remove lock
rm $LOCK

fi